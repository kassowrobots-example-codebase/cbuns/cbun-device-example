#include <iostream> 
#include <kr2_program_api/api_v1/program_interface.h>
#include <device_example/my_device.h>

namespace kswx_test {

    /*
     * The TestDevice class is used to make the protected activate and deactivate methods accessible. 
     */
    class TestDevice : public kswx_template::MyDevice {

    public: 
        
        TestDevice(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api, const boost::property_tree::ptree &a_xml_bundle_node)
        :   kswx_template::MyDevice(a_api, a_xml_bundle_node)
{}

        virtual ~TestDevice() {}

        void provideActivation(const boost::property_tree::ptree &a_param_tree) {
            activate(a_param_tree);
        }

        void provideDeactivation() {
            deactivate();
        }

    };

}

int main(int argc, const char * argv[]) {

    using namespace boost::property_tree;

    // Initialize API.
    auto api = kr2_program_api::ProgramInterface::factorySingletonInterface({});

    // Prepare instance property tree (provided by tablet once the device is added).
    ptree instance_tree;
    instance_tree.put("<xmlattr>.label", "DEV");                     
    instance_tree.put("<xmlattr>.cbun", "DeviceExampleCBun");        
    instance_tree.put("<xmlattr>.class", "kswx_template::MyDevice"); 

    // Create instance of your device. 
    auto *device = new kswx_test::TestDevice(api, instance_tree);

    // Call onCreate to simulate master instance behaviour. 
    device->onCreate();
    
    // Prepare activation property tree (provided by tablet once the device is activated).
    // The following params are supported:
    //   - boolean
    //   - int
    //   - double
    //   - string
    ptree param1;
    param1.put("string", "my_address");
    ptree activation_tree;
    activation_tree.add_child("param", param1);       

    // Provide activation (via TestDevice in order to expose the activate method)  
    device->provideActivation(activation_tree);

    // Test your device method/s.
    device->grip(5);

    // Provide deactivation (via TestDevice in order to expose the deactivate method)
    device->provideDeactivation();

    // Call onDestroy to simulate master instance behaviour.
    device->onDestroy();

    return 0;
}