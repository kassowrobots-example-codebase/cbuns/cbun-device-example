#include <kr2_program_api/api_v1/bundles/custom_device.h>

namespace kswx_template {

    /**
     * MyDevice class represents an abstraction of your custom device and therefore it is 
     * inherited from the kr2_bundle_api::CustomDevice class. It implements all the mandatory
     * custom device lifecycle methods (onCreate/onDestroy and onBind/onUnbind). It also 
     * demonstrates how the activation and mounting of the device should be provided via 
     * onActivate/onDeactivate and onMount/onUnmount methods. Above that it subscribes some 
     * of the system event callbacks (onHWReady, onProgramTerminated). And finally it shows
     * how custom methods (grip) and custom functions (getGripPosition) can be defined.
     */
    class MyDevice : public kr2_bundle_api::CustomDevice {
    public:
        
        /** @brief MyDevice class constructor.
         * @details Each CBun base class is provided with pointer to Program API and
         * CBun instance property tree that was used for instance creation.
         *
         * @param[in] api pointer to Program API
         * @param[in] node CBun instance property tree
         */
        MyDevice(boost::shared_ptr<kr2_program_api::ProgramInterface> api,
                           const boost::property_tree::ptree &xml_bundle_node);


        /** @brief MyDevice class destructor.
         *  
         */
        virtual ~MyDevice() = default;

        
        /*
         * Mandatory instance lifecycle methods. 
         */
        
        /** @brief Called when the CBun master instance is created.
         * @details This method is called only for master instance once it is
         * destroyed. This happens once the user add the CBun instance into the Workcell.
         *
         * @return 0 on success or error code
         */
        virtual int onCreate();

        /** @brief Called when the CBun master instance is destroyed.
         * @details This method is called only for master instance once it is
         * created. This happens once the user removes the CBun instance from the Workcell.
         *
         * @return 0 on success or error code
         */
        virtual int onDestroy();
        
        /** @brief Called when the CBun secondary instance is created.
         * @details This method is called only for secondary instances once they
         * are created. This happens when the user launches the program cluster.
         *
         * @return 0 on success or error code
         */
        virtual int onBind();

        /** @brief Called when the CBun secondary instance is destroyed.
         * @details This method is called only for secondary instances once they
         * are destroyed. This happens when the user terminates the program cluster.
         *
         * @return 0 on success or error code
         */
        virtual int onUnbind();
        
        /*
         * Optional system event callbacks.
         *
         * See Event Bus section at https://docs.kassowrobots.com/cbuns/development/CBun-Device.
         */
        
        void onHWReady(const kr2_signal::HWReady&);

        void onProgramTerminated(const kr2_signal::ProgramTerminated&);

        /*
         * Custom methods of the device. These methods can be used in Program Tree and therefore called during the 
         * program execution. Some of the methods (if allowed in bundle.xml) might be also called via XML-RPC 
         * (method jogging). Note that the method name and its params have to be aligned with the method description 
         * in the bundle.xml file.
         * 
         * See Published Methods section at https://docs.kassowrobots.com/cbuns/development/CBun-Device.
         */

        virtual CBUN_PCALL grip(int position);

        /*
         * Custom functions of the device. These functions can be used in Expression Builder and therefore evaluated 
         * during the program execution. Note that the function name, its params and return value has to be aligned 
         * with the function description in the bundle.xml file.
         * 
         * See Published Functions section at https://docs.kassowrobots.com/cbuns/development/CBun-Device.  
         */

        virtual kr2_program_api::Number getGripPosition();
        
    protected:
        
        /** @brief Called on device activation.
         * @details This method is called once the user presses the activate button. It should process the collected 
         * activation params (in form of param tree) and provide the activation of the device (enable power supply, 
         * establish communication channel, etc).
         */
        virtual CBUN_PCALL onActivate(const boost::property_tree::ptree &param_tree);

        /** @brief Called on device deactivation.
         * @details This method is called once the user presses the deactivate button. It should provide the deactivation
         * of the device (close communication channel, disable power supply, etc).
         */
        virtual CBUN_PCALL onDeactivate();
        
        /** @brief Called on device mount.
         * @details This method is called once the user presses the mount button. It should process the collected
         * mounting params (in form of param tree) and provide the mounting of the device (configure TCP and Tool Load).
         */
        virtual CBUN_PCALL onMount(const boost::property_tree::ptree &param_tree);

        /** @brief Called on device unmout.
         * @details This method is called once the user presses the unmount button. It should provide the unmounting 
         * of the device (clear TCP and Tool Load).
         */
        virtual CBUN_PCALL onUnmount();
    
    private:

        /*
         * Place you private (helper) methods here.
         */
        
    };
    
}
