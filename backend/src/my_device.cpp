#include <device_example/my_device.h>
#include <kr2_program_api/api_v1/bundles/arg_provider_xml.h>

using namespace kswx_template;

// The class has to be registered, otherwise the robot user will not be able
// to create an instance of the device (ie. he would not be able to Apply it).
REGISTER_CLASS(kswx_template::MyDevice)

MyDevice::MyDevice(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api,
                                       const boost::property_tree::ptree &a_xml_bundle_node)
:   kr2_bundle_api::CustomDevice(a_api, a_xml_bundle_node)
{}


int MyDevice::onCreate()
{
    // HWReady signal can be used to automatically activate the device once the robot is
    // initialized. This is helpful, if the device uses the ToolIO (for power supply or
    // communication). In such case, it should be subscribed in onCreate method so the 
    // activation is provided on the master instance.
    SUBSCRIBE(kr2_signal::HWReady, MyDevice::onHWReady);
    
    return 0;
}

int MyDevice::onDestroy()
{
    // It is a good practice to automatically deactivate the device in onDestroy method 
    // (ie. once the device is removed from Workcell or the robot is turned off).
    onDeactivate();

    return 0;
}

int MyDevice::onBind()
{
    // Since ProgramTerminated signal is provided only to secondary instances, it has
    // to be subcribed in onBind method. The callback should be used to interrupt any 
    // ongoing device methods to allow immediate program termination. 
    SUBSCRIBE(kr2_signal::ProgramTerminated, MyDevice::onProgramTerminated);
    
    // The activation tree is available if the device was successfully activate via
    // its master instance. 
    if (!activation_tree_) {
        CLOG_ERR("[CBUN/sample] Activation params are not available");
        return -1;
    }

    // Process the activation params (by using ArgProviderXml). The number of params,
    // as well as their order and data types have to be aligned with the config section
    // of the bundle.xml file. 
    kr2_bundle_api::ArgProviderXml arg_provider(*activation_tree_);
    std::string device_address = arg_provider.getString(0);

    // TODO: Provide the setup of the secondary instance.

    return 0;
}

int MyDevice::onUnbind()
{
    // TODO: Provide the clean of the secondary instance. 

    return 0;
}


void MyDevice::onHWReady(const kr2_signal::HWReady&)
{
    // This is an example of the auto device activate once the robot is initialized. 
    if (activation_tree_) {
        kr2_program_api::CmdResult<> result = activate(*activation_tree_);
        switch (result.result_) {
            case kr2_program_api::CmdResult<>::EXCEPTION:
                PUBLISH_EXCEPTION(result.code_, result.message_)
                break;
            case kr2_program_api::CmdResult<>::ERROR:
                PUBLISH_ERROR(result.code_, result.message_)
                break;
        }
    }
}


void MyDevice::onProgramTerminated(const kr2_signal::ProgramTerminated&term)
{
    // TODO: Interrupt any ongoing device methods.
}


CBUN_PCALL MyDevice::onActivate(const boost::property_tree::ptree &a_param_tree)
{

    // Process the activation params (by using ArgProviderXml). The number of params,
    // as well as their order and data types have to be aligned with the config section
    // of the bundle.xml file. 
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    std::string device_address = arg_provider.getString(0);
    CLOG_INFO("onActivate: device_address=" << device_address);

    // TODO: Provide the activation of the device. You should enable the power 
    // supply, open the communication channel and initialize the device itself. 
    
    // onActivate method has to end with CBUN_PCALL_RET_OK, CBUN_PCALL_RET_ERROR or 
    // CBUN_PCALL_RET_EXCEPTION.
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MyDevice::onDeactivate()
{
    // TODO: Provide the deactivation of the device. You should close the communication
    // channel and disable the power supply. 

    // onDeactivate method has to end with CBUN_PCALL_RET_OK, CBUN_PCALL_RET_ERROR or 
    // CBUN_PCALL_RET_EXCEPTION.
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MyDevice::onMount(const boost::property_tree::ptree &a_param_tree) {

    // Process the mounting params (by using ArgProviderXml). The number of params,
    // as well as their order and data types have to be aligned with the mounting section
    // of the bundle.xml file. 
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    kr2_program_api::Load load = arg_provider.getLoad(0);

    // Set the system tool load
    auto toolload = api_->variables_->allocSystemLoad("toolload", kr2rc_api::Load::SysId::LOAD_TOOL);
    *toolload = load;

    // onMount method has to end with CBUN_PCALL_RET_OK, CBUN_PCALL_RET_ERROR or 
    // CBUN_PCALL_RET_EXCEPTION.
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MyDevice::onUnmount() {

    // TODO: Implement unmount (ie. clear the TCP and Tool Load) or keep the method
    // empty if the mounting is not user (for example when the device is not mounted
    // on the robot). 
    
    // onMount method has to end with CBUN_PCALL_RET_OK, CBUN_PCALL_RET_ERROR or 
    // CBUN_PCALL_RET_EXCEPTION.
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL MyDevice::grip(int a_position)
{
    // TODO: Implement the grip method. This is usually done by direct commanding of 
    // the device via the opened communication channel. In some cases, when a single
    // communication channel can be established, you will need to forward the request 
    // to the master instance (for example via the IPC queue) and let the master 
    // instance to command the device. 

    // Each device method has to end with CBUN_PCALL_RET_OK, CBUN_PCALL_RET_ERROR or 
    // CBUN_PCALL_RET_EXCEPTION.
    CBUN_PCALL_RET_OK;
}

kr2_program_api::Number MyDevice::getGripPosition()
{
    // TODO: Implement the getGripPosition function. This is usually done by direct 
    // monitoring of the device via the opened communication channel. In some cases, 
    // when only a single communication channel can be established, you will need to 
    // forward the request to the master instance (for example via the IPC queue)
    // and let the master instance to monitor the device. 

    return kr2_program_api::Number(0L);
}
